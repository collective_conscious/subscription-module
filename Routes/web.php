<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::prefix('billing')->group(function () {

    Route::prefix('subscription')->group(function () {
        Route::prefix('plan')->group(function () {
            Route::group(['middleware' => ['can:View_Subscription_Plan']], function () {
                Route::get('/', 'SubscriptionPlanController@index')->name('subscription.plan.index');
                Route::post('/ajax', 'SubscriptionPlanController@ajax')->name('subscription.plan.ajax');
            });

            Route::group(['middleware' => ['can:Add_Subscription_Plan']], function () {
                Route::get('/create', 'SubscriptionPlanController@create')->name('subscription.plan.create');
                Route::post('/', 'SubscriptionPlanController@store')->name('subscription.plan.store');
            });

            Route::group(['middleware' => ['can:Get_Subscription_Plan']], function () {
                Route::get('{id}/view', 'SubscriptionPlanController@view')->name('subscription.plan.view');
            });

            Route::group(['middleware' => ['can:Edit_Subscription_Plan']], function () {
                Route::get('{id}/edit', 'SubscriptionPlanController@edit')->name('subscription.plan.edit');
                Route::patch('{id}', 'SubscriptionPlanController@update')->name('subscription.plan.update');
            });

            Route::group(['middleware' => ['can:Delete_Subscription_Plan']], function () {
                Route::delete('/', 'SubscriptionPlanController@destroy')->name('subscription.plan.destroy');
            });

        });
    });

});
