<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 11/5/2019
 * Time: 11:37 AM
 */

namespace Modules\Subscription\Repositories;


use CollectiveConscious\RepositoryDesignPattern\Repository;
use Modules\Subscription\Entities\SubscriptionPlan;

class SubscriptionPlanRepository extends Repository
{
    public function model()
    {
        return SubscriptionPlan::class;
    }
}