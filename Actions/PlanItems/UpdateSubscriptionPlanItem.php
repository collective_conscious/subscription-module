<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 11/5/2019
 * Time: 5:04 PM
 */

namespace Modules\Subscription\Actions\PlanItems;


use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Modules\Subscription\Repositories\SubscriptionPlanItemRepository;

class UpdateSubscriptionPlanItem
{
    private $repository;
    private $createSubscriptionPlanItem;

    public function __construct(SubscriptionPlanItemRepository $repository, CreateSubscriptionPlanItem $createSubscriptionPlanItem)
    {
        $this->repository = $repository;

        $this->createSubscriptionPlanItem = $createSubscriptionPlanItem;
    }

    /**
     * @param $planId
     * @param array $data
     * @return null
     * @throws \CollectiveConscious\RepositoryDesignPattern\Exceptions\RepositoryException
     */
    public function execute($planId, array $data) {

        if (Arr::has($data, 'subscriptionPlanItemId')) {

            $condition = $data['subscriptionPlanItemId'];

            foreach ($condition as $key => $value) {

                if($data['subscriptionPlanItemId'][$key] > 0) {

                    $this->repository->update([
                        'module' => $data['subscriptionPlanItemModule'][$key],
                        'title' => $data['subscriptionPlanItemTitle'][$key],
                        'description' => $data['subscriptionPlanItemDescription'][$key],
                        'limit_type' => $data['subscriptionPlanItemLimitType'][$key],
                        'limits' => $data['subscriptionPlanItemLimit'][$key],
                        'color' => $data['subscriptionPlanItemColor'][$key],
                        'icon' => $data['subscriptionPlanItemIcon'][$key],
                    ], $data['subscriptionPlanItemId'][$key]);
                }
            }

            // for new items
            $this->createSubscriptionPlanItem->execute($planId, $data);
        }
    }
}