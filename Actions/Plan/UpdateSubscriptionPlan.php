<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 11/7/2019
 * Time: 12:44 PM
 */

namespace Modules\Subscription\Actions\Plan;


use CollectiveConscious\RepositoryDesignPattern\Exceptions\RepositoryException;
use Illuminate\Support\Str;
use Modules\Subscription\Actions\PlanItems\UpdateSubscriptionPlanItem;
use Modules\Subscription\Actions\PlanModules\UpdateSubscriptionPlanModule;
use Modules\Subscription\Repositories\SubscriptionPlanRepository;

class UpdateSubscriptionPlan
{
    private $repository;
    private $updateSubscriptionPlanItem;
    private $updateSubscriptionPlanModule;

    public function __construct(SubscriptionPlanRepository $repository, UpdateSubscriptionPlanItem $updateSubscriptionPlanItem, UpdateSubscriptionPlanModule $updateSubscriptionPlanModule)
    {
        $this->repository = $repository;

        $this->updateSubscriptionPlanItem = $updateSubscriptionPlanItem;
        $this->updateSubscriptionPlanModule = $updateSubscriptionPlanModule;
    }

    /**
     * @param array $data
     * @param $id
     * @return mixed
     * @throws RepositoryException
     */
    public function execute(array $data, $id) {
        $plan = $this->repository->update([
            'title' => $data['title'],
            'name' => Str::slug($data['name'], '_'),
            'currency' => $data['currency'],
            'amount' => $data['amount'],
            'duration' => $data['duration'],
            'color' => $data['color'],
            'is_popular' => intval($data['is_popular']) ?? 0,
        ], $id);

        $this->updateSubscriptionPlanItem->execute($plan->id, $data);
        $this->updateSubscriptionPlanModule->execute($plan->id, $data);

        return $plan;
    }
}