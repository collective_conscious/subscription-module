<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 11/5/2019
 * Time: 5:06 PM
 */

namespace Modules\Subscription\Actions\Plan;


use Modules\Subscription\Repositories\SubscriptionPlanRepository;

class DeleteMultipleSubscriptionPlans
{
    private $repository;

    public function __construct(SubscriptionPlanRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $allChecked
     * @throws \CollectiveConscious\RepositoryDesignPattern\Exceptions\RepositoryException
     */
    public function execute(array $allChecked) {
        foreach ($allChecked as $id) {
            $this->repository->delete($id);
        }
    }
}