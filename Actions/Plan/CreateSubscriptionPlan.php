<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 11/5/2019
 * Time: 5:04 PM
 */

namespace Modules\Subscription\Actions\Plan;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Modules\Subscription\Actions\PlanItems\CreateSubscriptionPlanItem;
use Modules\Subscription\Actions\PlanModules\CreateSubscriptionPlanModule;
use Modules\Subscription\Repositories\SubscriptionPlanRepository;

class CreateSubscriptionPlan
{
    private $repository;
    private $createSubscriptionPlanItem;
    private $createSubscriptionPlanModule;

    public function __construct(SubscriptionPlanRepository $repository, CreateSubscriptionPlanItem $createSubscriptionPlanItem, CreateSubscriptionPlanModule $createSubscriptionPlanModule)
    {
        $this->repository = $repository;

        $this->createSubscriptionPlanItem = $createSubscriptionPlanItem;
        $this->createSubscriptionPlanModule = $createSubscriptionPlanModule;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws \CollectiveConscious\RepositoryDesignPattern\Exceptions\RepositoryException
     */
    public function execute(array $data) {
        $plan = $this->repository->create([
            'unique_id' => Str::uuid()->toString(),
            'user_id' => Auth::id(),
            'title' => $data['title'],
            'name' => Str::slug($data['name'], '_'),
            'currency' => $data['currency'],
            'amount' => $data['amount'],
            'duration' => $data['duration'],
            'color' => $data['color'],
            'is_popular' => intval($data['is_popular']) ?? 0,
        ]);

        $this->createSubscriptionPlanItem->execute($plan->id, $data);
        $this->createSubscriptionPlanModule->execute($plan->id, $data);

        return $plan;
    }
}