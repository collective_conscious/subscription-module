<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 11/5/2019
 * Time: 5:04 PM
 */

namespace Modules\Subscription\Actions\PlanModules;


use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Modules\Subscription\Repositories\SubscriptionPlanModuleRepository;

class UpdateSubscriptionPlanModule
{
    private $repository;
    private $createSubscriptionPlanModule;

    public function __construct(SubscriptionPlanModuleRepository $repository, CreateSubscriptionPlanModule $createSubscriptionPlanModule)
    {
        $this->repository = $repository;

        $this->createSubscriptionPlanModule = $createSubscriptionPlanModule;
    }

    /**
     * @param $planId
     * @param array $data
     * @return mixed
     * @throws \CollectiveConscious\RepositoryDesignPattern\Exceptions\RepositoryException
     */
    public function execute($planId, array $data) {
        if (Arr::has($data, 'modulesCheckbox')) {
            $this->repository->deleteWhere(['subscription_plan_id' => $planId]);
            $this->createSubscriptionPlanModule->execute($planId, $data);
        }
    }
}