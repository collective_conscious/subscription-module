<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 11/5/2019
 * Time: 5:04 PM
 */

namespace Modules\Subscription\Actions\PlanModules;


use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Modules\Subscription\Repositories\SubscriptionPlanModuleRepository;

class CreateSubscriptionPlanModule
{
    private $repository;

    public function __construct(SubscriptionPlanModuleRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param $planId
     * @param array $data
     * @return mixed
     * @throws \CollectiveConscious\RepositoryDesignPattern\Exceptions\RepositoryException
     */
    public function execute($planId, array $data) {
        if (Arr::has($data, 'modulesCheckbox')) {

            $condition = $data['modulesCheckbox'];

            foreach ($condition as $key => $value) {
                $this->repository->create([
                    'subscription_plan_id' => $planId,
                    'module' => $data['modulesCheckbox'][$key],
                    'limit_type' => $data['modulesLimitType'][$key],
                    'limits' => $data['modulesLimit'][$key],
                ]);
            }

        }
    }
}