<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 11/5/2019
 * Time: 5:14 PM
 */

namespace Modules\Subscription\Traits;


trait SubscriptionPlanTrait
{
    public function getSubscriptionPlanStatus($status) {
        switch ($status) {
            case 0:
                return '<span class="label label-danger">In-Active</span>';
            case 1:
                return '<span class="label label-success">Active</span>';
            case 2:
                return '<span class="label label-info">Promo Active</span>';

            default:
                return '-';
        }
    }
}