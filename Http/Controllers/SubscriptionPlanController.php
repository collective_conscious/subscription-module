<?php

namespace Modules\Subscription\Http\Controllers;

use Carbon\Carbon;
use CollectiveConscious\RepositoryDesignPattern\Exceptions\RepositoryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Modules\Subscription\Actions\Plan\CreateSubscriptionPlan;
use Modules\Subscription\Actions\Plan\DeleteMultipleSubscriptionPlans;
use Modules\Subscription\Actions\Plan\UpdateSubscriptionPlan;
use Modules\Subscription\Http\Requests\SubscriptionPlanRequest;
use Modules\Subscription\Repositories\SubscriptionPlanItemRepository;
use Modules\Subscription\Repositories\SubscriptionPlanModuleRepository;
use Modules\Subscription\Repositories\SubscriptionPlanRepository;
use App\Http\Controllers\Controller;
use Modules\Subscription\Traits\SubscriptionPlanTrait;
use Modules\UserManagement\Traits\AbilityTrait;

class SubscriptionPlanController extends Controller
{
    use SubscriptionPlanTrait, AbilityTrait;

    private $subscriptionPlanRepository;

    /**
     * Create a new controller instance.
     *
     * @param SubscriptionPlanRepository $subscriptionPlanRepository
     */
    public function __construct(SubscriptionPlanRepository $subscriptionPlanRepository)
    {
        $this->middleware(['auth']);

        $this->subscriptionPlanRepository = $subscriptionPlanRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return response()->json([
                'success' => true,
                'code' => 200,
                'view' => view('subscription::plan.content')->render(),
            ]);
        }
        else
            return view('subscription::plan.index');
    }

    /**
     * Send records to client based on ajax for long records
     *
     * @param Request $request
     * @return void
     * @throws RepositoryException
     */
    public function ajax(Request $request)
    {
        $columns = array(
            0 =>'id',
            1 => 'duration',
            2 => 'title',
            3 => 'name',
            4 => 'unique_id',
            5 => 'amount',
            6 => 'status',
            7 => 'updated_at',
        );

        $items = $this->subscriptionPlanRepository->all();

        $totalData = $items->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            if($request->input('order.0.column') == 7)
                $collection = $items->slice($start, $limit)->sortByDate($order, ($dir === 'asc')?true:false);
            else
                $collection = $items->slice($start, $limit)->sortBy($order, ($dir === 'asc')?true:false);
        }
        else {
            $search = $request->input('search.value');

            if($request->input('order.0.column') == 7) {
                $collection = $items->filter(function ($item) use ($search) {
                    if (stripos($item->name, $search) !== false || stripos($item->title, $search) !== false || stripos($item->duration, $search) !== false)
                        return $item;
                })
                    ->slice($start, $limit)
                    ->sortByDate($order, ($dir === 'asc')?true:false);
            }
            else {
                $collection = $items->filter(function ($item) use ($search) {
                    if (stripos($item->name, $search) !== false || stripos($item->title, $search) !== false || stripos($item->duration, $search) !== false)
                        return $item;
                })
                    ->slice($start, $limit)
                    ->sortBy($order, ($dir === 'asc')?true:false);
            }


            $totalFiltered = $items->filter(function ($item) use ($search) {
                if (stripos($item->name, $search) !== false || stripos($item->title, $search) !== false || stripos($item->duration, $search) !== false)
                    return $item;
            })
                ->count();
        }

        $data = array();
        if(!empty($collection))
        {
            $i = $start + 1;

            foreach ($collection as $item)
            {
                $nestedData['check'] = '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                            <input type="checkbox" value="'.$item->id.'" class="checkboxes" data-set="#sample_1 .checkboxes" />
                                            <span></span>
                                        </label>';

                $action = '<a class="btn btn-icon-only green popovers" data-container="body" data-trigger="hover" data-placement="auto"
                                    data-content="View Subscription Plan detail." data-original-title="View Subscription Plan"
                                    data-toggle="modal" data-target="#InputModal" data-modaltitle="View Subscription Plan" data-modaltype="edit" data-company="'. ((Auth::user()->company == null)?null:((Auth::user()->company == null)?Auth::user()->company:null)) .'" data-record="'.$item->id.'" data-layouttoadd="billing/subscription/plan/'.$item->id.'/view" data-module="subscription_plan" data-btnconfirm="View Subscription Plan"
                                >
                                    <i class="fa fa-binoculars"></i>
                                </a>';;

                if(Auth::user()->can('Edit_Subscription_Plan')) {
                    $action .= '<a class="btn btn-icon-only blue popovers" data-container="body" data-trigger="hover" data-placement="auto"
                                    data-content="Edit Subscription Plan detail." data-original-title="Edit Subscription Plan"
                                    data-toggle="modal" data-target="#InputModal" data-modaltitle="Edit Subscription Plan" data-modaltype="edit" data-company="'. ((Auth::user()->company == null)?null:((Auth::user()->company == null)?Auth::user()->company:null)) .'" data-record="'.$item->id.'" data-layouttoadd="billing/subscription/plan/'.$item->id.'/edit" data-module="subscription_plan" data-btnconfirm="Edit Subscription Plan"
                                >
                                    <i class="fa fa-edit"></i>
                                </a>';
                }


                $nestedData['duration'] = $item->duration;
                $nestedData['title'] = $item->title;
                $nestedData['name'] = $item->name;
                $nestedData['unique_id'] = $item->unique_id;
                $nestedData['amount'] = $item->curreny . ' ' . $item->amount;
                $nestedData['status'] = $this->getSubscriptionPlanStatus($item->status);
                $nestedData['added'] = Carbon::parse($item->created_at)->diffForHumans();
                $nestedData['action'] = $action;

                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function create() {
        $modules = $this->getTenantApplicationModulesArray();

        $currencies = [
            '' => 'Select Currency',
            'PKR' => 'PKR'
        ];

        $durationArray = [
            '' => 'Select Duration/Occurrence',
            'monthly' => 'Monthly',
            'quarterly' => 'Quarterly',
            'yearly' => 'Yearly',
        ];


        return response()->json([
            'success' => true,
            'code' => 200,
            'view' => view('subscription::plan.create', compact('modules', 'currencies', 'durationArray'))->render()
        ]);
    }

    /**
     * @param SubscriptionPlanRequest $request
     * @param CreateSubscriptionPlan $action
     * @return \Illuminate\Http\JsonResponse
     * @throws RepositoryException
     */
    public function store(SubscriptionPlanRequest $request, CreateSubscriptionPlan $action)
    {
        $action->execute($request->all());

        Cache::forget('subscription:plan:all');

        return response()->json([
            'success' => true,
            'code' => 200,
            'msg' => 'Subscription Plan has been added.',
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws RepositoryException
     */
    public function view($id)
    {
        $plan = $this->subscriptionPlanRepository->with(['modules', 'items'])->find($id);

        return response()->json([
            'success' => true,
            'code' => 200,
            'view' => view('subscription::plan.view', compact('plan'))->render()
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws RepositoryException
     */
    public function edit($id)
    {
        $plan = $this->subscriptionPlanRepository->with(['modules', 'items'])->find($id);
        $modules = $this->getTenantApplicationModulesArray();

        $currencies = [
            '' => 'Select Currency',
            'PKR' => 'PKR'
        ];

        $durationArray = [
            '' => 'Select Duration/Occurrence',
            'monthly' => 'Monthly',
            'quarterly' => 'Quarterly',
            'yearly' => 'Yearly',
        ];

        return response()->json([
            'success' => true,
            'code' => 200,
            'view' => view('subscription::plan.edit', compact('plan', 'modules', 'currencies', 'durationArray'))->render()
        ]);
    }

    /**
     * @param $id
     * @param SubscriptionPlanRequest $request
     * @param UpdateSubscriptionPlan $action
     * @return \Illuminate\Http\JsonResponse
     * @throws RepositoryException
     */
    public function update($id, SubscriptionPlanRequest $request, UpdateSubscriptionPlan $action)
    {
        try {

            $plan = $this->subscriptionPlanRepository->find($id);

        } catch (QueryException $exception) {
            return response()->json([
                'success' => false,
                'code' => 400,
                'msg' => 'Plan not found.',
            ], 400);
        }

        $action->execute($request->all(), $plan->id);

        Cache::forget('subscription:plan:all');

        return response()->json([
            'success' => true,
            'code' => 200,
            'msg' => 'Plan has been updated.',
            'request' => $request->all()
        ]);
    }

    /**
     * Destroy the given permission.
     *
     * @param Request $request
     * @param DeleteMultipleSubscriptionPlans $action
     * @return Response
     */
    public function destroy(Request $request, DeleteMultipleSubscriptionPlans $action)
    {
        if ($request->get('AllChecked') != null){

            $allChecked = $request->get('AllChecked');

            try {
                $action->execute($allChecked);

                Cache::forget('subscription:plan:all');

                return response()->json([
                    'success' => true,
                    'msg' => 'All selected subscription Plans have been deleted.'
                ]);
            }
            catch (\Exception $exception) {
                return response()->json([
                    'success' => false,
                    'code' => 400,
                    'msg' => $exception->getMessage(),
                ], 422);
            }
        }else {
            return response()->json([
                'success' => false,
                'code' => 400,
                'msg' => 'You have to select some item from table showing below.'
            ], 422);
        }
    }
}
