<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 11/6/2019
 * Time: 11:17 AM
 */

namespace Modules\Subscription\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubscriptionPlanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                        'title' => 'required',
                        'name' => 'required',
                        'duration' => 'required',
                    ];
                }
            case 'PUT':
            case 'PATCH':
                {
                    return [
                        'title' => 'required',
                        'name' => 'required',
                        'duration' => 'required',
                    ];
                }
            default:
                return [];
        }
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'Title, which will show on the top, is required.',
            'name.required' => 'Title, which will be used in coding, is required.',
            'duration.required' => 'Duration/Occurrence is required. Monthly, Yearly',
        ];
    }
}
