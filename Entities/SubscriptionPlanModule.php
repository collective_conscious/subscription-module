<?php

namespace Modules\Subscription\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class SubscriptionPlanModule extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subscription_plan_id', 'module', 'limit_type', 'limits'
    ];

    public function subscriptionPlan() {
        return $this->belongsTo(SubscriptionPlan::class, 'subscription_plan_id');
    }
}
