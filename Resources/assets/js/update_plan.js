/**
 * Created by Bilal Younas on 5/21/2017.
 */

$('.modules').on('change', function () {
    if($(this).prop('checked')){
        $('#AddOneSubscriptionPlanItem').click();
    }
    else {
        $('#RemoveOneSubscriptionPlanItem').click();
    }

});

$('#AddOneSubscriptionPlanItem').on('click', function () {
    var totalSubscriptionPlanItem = $('#totalSubscriptionPlanItem').val();

    if(totalSubscriptionPlanItem >= 50)
        return;

    if(totalSubscriptionPlanItem === 0)
        $('#tableSubscriptionPlanItemLocalBody').html(subscriptionPlanItemTemplate(parseInt(totalSubscriptionPlanItem)+1));
    else
        $('#rowSubscriptionPlanItem_'+totalSubscriptionPlanItem).after(subscriptionPlanItemTemplate(parseInt(totalSubscriptionPlanItem)+1));

    $('#totalSubscriptionPlanItem').val(parseInt(totalSubscriptionPlanItem)+1);
    $('#totalSubscriptionPlanItemSelect').val($('#totalSubscriptionPlanItem').val());

    initSubscriptionPlanItemEvents();
});

$('#RemoveOneSubscriptionPlanItem').on('click', function () {
    var totalSubscriptionPlanItem = $('#totalSubscriptionPlanItem').val();

    if(totalSubscriptionPlanItem > 1) {

        $('#rowSubscriptionPlanItem_' + totalSubscriptionPlanItem).remove();
        $('#totalSubscriptionPlanItem').val(parseInt(totalSubscriptionPlanItem) - 1);
        $('#totalSubscriptionPlanItemSelect').val($('#totalSubscriptionPlanItem').val());
    }
});

$('#totalSubscriptionPlanItemSelect').change(function () {
    var subscriptionPlanItemToBe = $(this).val();
    var totalSubscriptionPlanItem = parseInt($('#totalSubscriptionPlanItem').val());

    if(subscriptionPlanItemToBe < 1 || subscriptionPlanItemToBe > 51)
        return;

    if(subscriptionPlanItemToBe > totalSubscriptionPlanItem){
        for (var i = parseInt(totalSubscriptionPlanItem)+1; i <= subscriptionPlanItemToBe; i++) {
            $('#rowSubscriptionPlanItem_' + totalSubscriptionPlanItem).after(subscriptionPlanItemTemplate(++totalSubscriptionPlanItem));
        }

        $('#totalSubscriptionPlanItem').val(parseInt(totalSubscriptionPlanItem));
    }
    else if(subscriptionPlanItemToBe < totalSubscriptionPlanItem){
        for (var i = totalSubscriptionPlanItem-1; i >= subscriptionPlanItemToBe; i--) {
            $('#rowSubscriptionPlanItem_' + totalSubscriptionPlanItem).remove();
            --totalSubscriptionPlanItem;
        }

        $('#totalSubscriptionPlanItem').val(parseInt(totalSubscriptionPlanItem));
    }

    initSubscriptionPlanItemEvents();
});

function subscriptionPlanItemTemplate(id) {
    var template = '<tr id="rowSubscriptionPlanItem_'+id+'" class="no-padding no-margins" style="margin-bottom: 0%">\n' +
        '\n' +
        '                                    <td>\n' +
        '                                        '+id+'\n' +
        '                                        <input type="hidden" name="subscriptionPlanItemId['+id+']" value="-1">\n' +
        '                                    </td>\n' +
        '\n' +
        '                                    <td class="no-margins no-padding" style="margin: 0; padding: 0;">\n' +
        '                                        <div class="form-group no-margins" style="margin: 0; padding: 0;">\n' +
        '                                            <select class="form-control subscriptionPlanItemModule select2" id="subscriptionPlanItemModule_'+id+'" name="subscriptionPlanItemModule['+id+']">\n';

                        $.each(modules, function (index, module) {
                            template += '<option value="'+index+'" >'+module+'</option>';
                        });

        template += '                                </select>\n' +
        '                                        </div>\n' +
        '                                    </td>\n' +
        '\n' +
        '                                    <td class="no-margins no-padding" style="margin: 0; padding: 0; width: 12%;">\n' +
        '                                        <div class="form-group no-margins" style="margin: 0; padding: 0;">\n' +
        '                                            <input id="subscriptionPlanItemTitle_'+id+'" name="subscriptionPlanItemTitle['+id+']" placeholder="Members" type="text" class="form-control subscriptionPlanItemTitle">\n' +
        '                                        </div>\n' +
        '                                    </td>\n' +
        '\n' +
        '                                    <td class="no-margins no-padding" style="margin: 0; padding: 0; width: 12%;">\n' +
        '                                        <div class="form-group no-margins" style="margin: 0; padding: 0;">\n' +
        '                                            <input id="subscriptionPlanItemDescription_'+id+'" name="subscriptionPlanItemDescription['+id+']" placeholder="5 Members" type="text" class="form-control subscriptionPlanItemDescription">\n' +
        '                                        </div>\n' +
        '                                    </td>\n' +
        '\n' +
        '                                    <td class="no-margins no-padding" style="margin: 0; padding: 0; width: 12%;">\n' +
        '                                        <div class="form-group no-margins" style="margin: 0; padding: 0;">\n' +
        '                                            <input id="subscriptionPlanItemLimitType_'+id+'" name="subscriptionPlanItemLimitType['+id+']" placeholder="Storage, Users etc." type="text" class="form-control subscriptionPlanItemLimitType">\n' +
        '                                        </div>\n' +
        '                                    </td>\n' +
        '\n' +
        '                                    <td class="no-margins no-padding" style="margin: 0; padding: 0; width: 12%;">\n' +
        '                                        <div class="form-group no-margins" style="margin: 0; padding: 0;">\n' +
        '                                            <input id="subscriptionPlanItemLimit_'+id+'" name="subscriptionPlanItemLimit['+id+']" placeholder="5 Users, 500GB" type="text" class="form-control subscriptionPlanItemLimit">\n' +
        '                                        </div>\n' +
        '                                    </td>\n' +
        '\n' +
        '                                    <td class="no-margins no-padding" style="margin: 0; padding: 0; width: 12%;">\n' +
        '                                        <div class="form-group no-margins" style="margin: 0; padding: 0;">\n' +
        '                                            <input id="subscriptionPlanItemColor_'+id+'" name="subscriptionPlanItemColor['+id+']" placeholder="Text Color" type="color" class="form-control subscriptionPlanItemColor">\n' +
        '                                        </div>\n' +
        '                                    </td>\n' +
        '\n' +
        '                                    <td class="no-margins no-padding" style="margin: 0; padding: 0; width: 12%;">\n' +
        '                                        <div class="form-group no-margins" style="margin: 0; padding: 0;">\n' +
        '                                            <input id="subscriptionPlanItemIcon_'+id+'" name="subscriptionPlanItemIcon['+id+']" placeholder="Font Awesome Icons" type="text" class="form-control subscriptionPlanItemIcon">\n' +
        '                                        </div>\n' +
        '                                    </td>\n' +
        '\n' +
        '                                </tr>';


    return template;
}

var initSubscriptionPlanItemEvents = function () {
    $('.select2').select2({
        width: '100%'
    });
}

initSubscriptionPlanItemEvents();




