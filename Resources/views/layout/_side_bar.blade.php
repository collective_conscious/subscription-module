@if($user->can('View_Subscription_Plan') )
    <li class="heading">
        <h3 class="uppercase">Billing & Accounts</h3>
    </li>
@endif

@if($user->can('View_Subscription_Plan'))
    <li class="navbar_ajax nav-item
              {{ (Request::is('billing/subscription/plan') ? 'active' : '') }}
        " id="NavBar_billing_subscription_plan">
        <a href="javascript:LoadView('billing/subscription/plan', 'get', null, true);" class="nav-link ">
            <i class="fa fa-shopping-cart"></i>
            <span class="title">Subscription Plan</span>
            <span id="NavBar_billing_subscription_plan" class="navbar_arrow
              {{ ((Request::is('billing/subscription/plan')) ? 'selected' : '') }}
            "
            ></span>
        </a>
    </li>
@endif