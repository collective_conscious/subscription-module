
    <div class="row">
        <div class="col-md-12">
            @include('errors.list')
            @include('errors.ajax_list')

            {!! Form::open(['url' => 'billing/subscription/plan', 'class' => '', 'id' => 'form_sample_2']) !!}
                @include('subscription::plan._form', [
                    'checkedModules' => [],
                    'items' => [],
                    'isPopular' => false,
                    'defaultEntries' => 1
                 ])
            {!! Form::close() !!}

        </div>
    </div>

    <script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('app/js/billing/subscription/update_plan.js?'.time()) }}" type="text/javascript"></script>