

<link href="{{ asset('assets/pages/css/pricing.css') }}" rel="stylesheet" type="text/css" />

<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-4  pricing-content-1">
        <div class="price-column-container border-active">
            <div class="price-table-head bg-blue">
                <h2 class="no-margin">{{ $plan->title }}</h2>
            </div>
            <div class="arrow-down border-top-blue"></div>
            <div class="price-table-pricing">
                <h3>
                    <sup class="price-sign">{{ $plan->currency }}</sup>{{ $plan->amount }}</h3>
                <p>per {{ $plan->duration }}</p>
            </div>
            <div class="price-table-content">
                @foreach($plan->items as $item)
                    <div class="row mobile-padding">
                        <div class="col-xs-3 text-right mobile-padding">
                            <i class="{{ $item->icon ?? 'fa fa-arrow-right' }}"></i>
                        </div>
                        <div class="col-xs-9 text-left mobile-padding">{{ $item->description }}</div>
                    </div>
                @endforeach
            </div>
            <div class="arrow-down arrow-grey"></div>
            <div class="price-table-footer">
                <button type="button" class="btn grey-salsa btn-outline sbold uppercase price-button">Sign Up</button>
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
    <div class="col-md-6">
        <div class="portlet light portlet-fit bordered">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-green bold uppercase">Selected Modules</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="portlet-body" style="overflow: hidden">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered table-scrollable">
                                <tr>
                                    <th>Module</th>
                                    <th>Limit Type</th>
                                    <th>Limit</th>
                                </tr>

                                @foreach($plan->modules as $module)
                                    <tr>
                                        <td>{{ ucwords(\Illuminate\Support\Str::replaceArray('_', [' '], $module->module)) }}</td>
                                        <td>{{ $module->limit_type ?? '-' }}</td>
                                        <td>{{ $module->limits ?? '-' }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>