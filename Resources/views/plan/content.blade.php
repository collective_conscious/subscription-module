

<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ url('home') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Subscription Plan</span>
        </li>
    </ul>
    <div class="page-toolbar">
        <div class="btn-group pull-right">
            <div class="btn-group btn-group-solid">
                @if(Auth::user()->can('Add_Subscription_Plan'))
                    <a  type="button" class="btn blue popovers" data-container="body" data-trigger="hover" data-placement="auto"
                        data-content="Role can be assign to the subscription Plan and also some permissions from that role." data-original-title="Add Subscription Plan"
                        data-toggle="modal" data-target="#InputModal" data-modaltitle="Add Subscription Plan" data-modaltype="add" data-company="{{ (Auth::user()->company ?? null ) }}" data-record="" data-layouttoadd="billing/subscription/plan/create" data-module="subscription_plan" data-btnconfirm="Add Subscription Plan"
                    >
                        <i class="fa fa-plus"></i>
                        Add Subscription Plan
                    </a>
                @endif
                @if(Auth::user()->can('Delete_Subscription_Plan'))
                    <button type="button" onclick="javascript:Delete('billing/subscription/plan', 'Subscription Plan will loose all the data.')" class="btn purple-studio popovers" data-container="body" data-trigger="hover" data-placement="auto"
                            data-content="Delete subscription Plan by selecting one or many from below." data-original-title="Delete Subscription Plan">
                        <i class="fa fa-trash"></i>
                        Delete Subscription Plans
                    </button>
                @endif
                <div class="btn-group pull-right">
                    <button class="btn red btn-outline dropdown-toggle popovers" data-container="body" data-trigger="hover" data-placement="auto"
                            data-content="You can print, copy or get a PDF, Excel or CSV of the data showing below." data-original-title="Tools" data-toggle="dropdown">
                        <i class="fa fa-share"></i>
                        <span class="hidden-xs"> Trigger Tools </span>
                        <i class="fa fa-angle-down"></i>
                    </button>
                    <ul class="dropdown-menu pull-right" id="sample_1_tools">
                        @if(Auth::user()->can('Print_Subscription_Plan'))
                            <li>
                                <a href="javascript:;" data-action="0" class="tool-action">
                                    <i class="icon-printer"></i> Print</a>
                            </li>
                            <li>
                                <a href="javascript:;" data-action="1" class="tool-action">
                                    <i class="icon-check"></i> Copy</a>
                            </li>
                            <li>
                                <a href="javascript:;" data-action="2" class="tool-action">
                                    <i class="icon-doc"></i> PDF</a>
                            </li>
                            <li>
                                <a href="javascript:;" data-action="3" class="tool-action">
                                    <i class="icon-paper-clip"></i> Excel</a>
                            </li>
                            <li>
                                <a href="javascript:;" data-action="4" class="tool-action">
                                    <i class="icon-cloud-upload"></i> CSV</a>
                            </li>
                            <li class="divider"> </li>
                        @endif
                        <li>
                            <a href="javascript:;" data-action="5" class="tool-action">
                                <i class="icon-refresh"></i> Clear Filters</a>
                        </li>
                    </ul>
                </div>
                <button class="btn dark btn-outline popovers" id="sample_1_tools_1" data-action="6" data-container="body" data-trigger="hover" data-placement="auto"
                        data-content="You can view or hide columns and print or download desired output." data-original-title="Column Visibility">
                    Columns
                </button>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> Subscription Plan
    <small>All the subscription plans, this company has are shown below.</small>
</h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->

<script type="text/javascript">
    LoadURL = '{{ url('billing/subscription/plan/ajax') }}';
    ShowAction = {{ (Auth::user()->can(['Edit_Subscription_Plan']))?'true':'false' }};
</script>

<div class="alert alert-success alert-dismissible hide" id="AjaxSuccessAlert">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span></span>
</div>

<div class="alert alert-danger alert-dismissible hide" id="AjaxMainErrorAlert">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span></span>
</div>

<div class="row">
    <div class="col-md-12">

        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover table-checkable order-column sample_1" id="tblSubscriptionPlan_{{ Auth::id() }}">
                    <thead>
                    <tr>
                        <th>
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                <input type="checkbox" class="group-checkable" data-set="#tblSubscriptionPlan_{{ Auth::id() }} .checkboxes" />
                                <span></span>
                            </label>
                        </th>
                        <th> Duration </th>
                        <th> Title </th>
                        <th> Name </th>
                        <th> Unique Id </th>
                        <th> Amount </th>
                        <th> Status </th>
                        <th> Added </th>
                        @if(Auth::user()->can(['Edit_Subscription_Plan']))
                            <th> Actions </th>
                        @endif
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>


<script src="{{ asset('app/js/billing/subscription/plan.js') }}" type="text/javascript"></script>