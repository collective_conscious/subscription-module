
<div class="row">
    <div class="col-md-12">
        @include('errors.list')
        @include('errors.ajax_list')
        <div class="portlet light bordered">
            <div class="portlet-body form">

                {!! Form::model($plan, ['method' => 'PATCH', 'class' => '', 'id' => 'form_sample_2', 'action' => ['\Modules\Subscription\Http\Controllers\SubscriptionPlanController@update', $plan->id]]) !!}
                @include('subscription::plan._form', [
                    'checkedModules' => $plan->modules->pluck('module', 'module'),
                    'checkedModulesLimitTypes' => $plan->modules->pluck('limit_type', 'module'),
                    'checkedModulesLimits' => $plan->modules->pluck('limits', 'module'),
                    'items' => $plan->items,
                    'isPopular' => $plan->is_popular,
                    'defaultEntries' => $plan->items->count()
                ])
                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>

<script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('app/js/billing/subscription/update_plan.js?'.time()) }}" type="text/javascript"></script>

<script type="text/javascript">
    $('input').addClass('edited');
</script>