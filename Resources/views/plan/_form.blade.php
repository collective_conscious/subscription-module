

<div class="row">
    <div class="col-md-12 no-padding">
        <div class="portlet light bordered">
            <div class="portlet-body form">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-7" style="border-right: solid 1px lightgray;">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-md-line-input form-md-floating-label {{ ($errors->has('title') ? 'has-error' : '') }}">
                                        {!! Form::text('title', null, array(
                                            'id' => 'title',
                                            'required' => 'required',
                                            'class' => 'form-control'
                                        )) !!}
                                        <label for="Title">Title <span style="color: red">*</span></label>
                                        <span class="help-block">Title of the Plan, Basic, Enterprise</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input form-md-floating-label {{ ($errors->has('name') ? 'has-error' : '') }}">
                                        {!! Form::text('name', null, array(
                                            'id' => 'name',
                                            'required' => 'required',
                                            'class' => 'form-control'
                                        )) !!}
                                        <label for="name">Name <span style="color: red">*</span></label>
                                        <span class="help-block">Short Name to be used in coding.</span>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input form-md-floating-label {{ ($errors->has('duration') ? 'has-error' : '') }}">
                                        {!! Form::select('duration', $durationArray, null, array(
                                            'id' => 'duration',
                                            'class' => 'form-control select2 edited'
                                        )) !!}
                                        <label for="duration">Duration/Occurrence </label>
                                        <span class="help-block">Select the duration from the list</span>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input form-md-floating-label {{ ($errors->has('currency') ? 'has-error' : '') }}">
                                        {!! Form::select('currency', $currencies, null, array(
                                            'id' => 'currency',
                                            'class' => 'form-control select2 edited'
                                        )) !!}
                                        <label for="currency">Currency </label>
                                        <span class="help-block">Select the currency from the list</span>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input form-md-floating-label {{ ($errors->has('amount') ? 'has-error' : '') }}">
                                        {!! Form::number('amount', null, array(
                                            'id' => 'amount',
                                            'step' => 'any',
                                            'class' => 'form-control'
                                        )) !!}
                                        <label for="amount">Amount</label>
                                        <span class="help-block">Amount to be charged for the time period of duration</span>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input form-md-floating-label {{ ($errors->has('color') ? 'has-error' : '') }}">
                                        {!! Form::color('color', null, array(
                                            'id' => 'color',
                                            'class' => 'form-control edited'
                                        )) !!}
                                        <label for="color">Title Background Color</label>
                                        <span class="help-block">Background color of the title and button</span>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6 no-padding no-margin">
                                    <div class="md-checkbox-inline">
                                        <div class="md-checkbox has-success">
                                            {!! Form::checkbox('is_popular', '1', $isPopular, array(
                                                'id' => 'is_popular',
                                                'class' => 'md-check '
                                            )) !!}
                                            <label for="is_popular">
                                                <span></span>
                                                <span class="check popovers" data-container="modal" data-trigger="hover" data-placement="auto"
                                                      data-content="Only select if this package is popular" data-original-title="Is popular"></span>

                                                <span class="box popovers" data-container="modal" data-trigger="hover" data-placement="auto"
                                                      data-content="Only select if this package is popular" data-original-title="Is popular"></span>

                                                Is Popular
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5" style="padding-top: 0;">
                            <h3 style="margin-top: 0;">Selected Modules</h3>
                            <hr>

                            @foreach($modules as $module => $module_title)
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="md-checkbox-inline">
                                            <div class="md-checkbox  has-success">
                                                {!! Form::checkbox('modulesCheckbox['.$module.']', $module, Arr::has($checkedModules, $module)?true:false, array(
                                                    'id' => $module,
                                                    'class' => 'md-check modules'
                                                )) !!}
                                                <label for="{{ $module }}">
                                                    <span></span>
                                                    <span class="check" ></span>
                                                    <span class="box"></span>
                                                    {{ $module_title }}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 no-padding no-margin">
                                        <div class="form-group form-md-line-input form-md-floating-label {{ ($errors->has('modules['.$module.'][limit_type]') ? 'has-error' : '') }}" style="margin: 0 !important;; padding: 0 !important;;">
                                            {!! Form::text('modulesLimitType['.$module.']', Arr::has($checkedModules, $module)?$checkedModulesLimitTypes[$module] ?? null:null, array(
                                                'id' => 'limit_type'.$module,
                                                'placeholder' => 'Limit Type',
                                                'class' => 'form-control'
                                            )) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3 no-padding no-margin">
                                        <div class="form-group form-md-line-input form-md-floating-label no-padding  {{ ($errors->has('modules['.$module.'][limit]') ? 'has-error' : '') }}"  style="margin: 0 !important;; padding: 0 !important;;">
                                            {!! Form::text('modulesLimit['.$module.']', Arr::has($checkedModules, $module)?$checkedModulesLimits[$module] ?? null:null, array(
                                                'id' => 'limit'.$module,
                                                'placeholder' => 'Limit',
                                                'class' => 'form-control'
                                            )) !!}
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 no-padding">
        <div class="portlet light bordered">
            <div class="portlet-title no-padding no-margin">
                <div class="caption font-blue-hoki">
                    <i class="fa fa-list font-blue-hoki"></i>
                    <span class="caption-subject bold uppercase"> Plan Items</span>
                    <span class="caption-helper"></span>
                </div>

                <div class="actions col-md-6">
                    <div class="btn-group pull-right">
                        <a class="btn btn-info btn-sm" data-toggle="tooltip" data-original-title="Add One Item" id="AddOneSubscriptionPlanItem" data-placement="left"><i class="fa fa-plus"></i>&nbsp;&nbsp;Add</a>
                        <a class="btn btn-danger btn-sm" data-toggle="tooltip" data-original-title="Remove Last Item" id="RemoveOneSubscriptionPlanItem" data-placement="left"><i class="fa fa-minus"></i>&nbsp;&nbsp;Remove</a>
                    </div>

                    <div class="col-sm-3 pull-right">
                        <select id="totalSubscriptionPlanItemSelect" class="form-control input-sm">
                            @for($i = 1; $i <= 50; $i++)
                                <option value="{{ $i }}" {{ ($i == $defaultEntries)?'selected':'' }}>{{ $i }}</option>
                            @endfor
                        </select>
                    </div>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="portlet-body form" style="margin-top: 0%">
                    <div class="form-body" style="padding-top: 0; overflow-x:auto;">
                        <table class="table  table-scrollable table-bordered" id="tableSubscriptionPlanItemLocal">
                            <thead>
                            <tr>
                                <th style="width: 3%">#</th>
                                <th>Module</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Limit Type</th>
                                <th>Limit</th>
                                <th>Color</th>
                                <th>Icon</th>
                            </tr>
                            </thead>
                            <tbody id="tableSubscriptionPlanItemLocalBody">

                            @if(count($items) == 0)
                                <tr id="rowSubscriptionPlanItem_1" class="no-padding no-margins" style="margin-bottom: 0%">

                                    <td>
                                        1
                                        <input type="hidden" name="subscriptionPlanItemId[1]" value="-1">
                                    </td>

                                    <td class="no-margins no-padding" style="margin: 0; padding: 0;">
                                        <div class="form-group no-margins" style="margin: 0; padding: 0;">
                                            <select class="form-control subscriptionPlanItemModule select2" id="subscriptionPlanItemModule_1" name="subscriptionPlanItemModule[1]">
                                                @foreach($modules as $module => $module_title)
                                                    <option value="{{ $module }}" >{{ $module_title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </td>

                                    <td class="no-margins no-padding" style="margin: 0; padding: 0; width: 12%;">
                                        <div class="form-group no-margins" style="margin: 0; padding: 0;">
                                            <input id="subscriptionPlanItemTitle_1" name="subscriptionPlanItemTitle[1]" placeholder="Members" type="text" class="form-control subscriptionPlanItemTitle">
                                        </div>
                                    </td>

                                    <td class="no-margins no-padding" style="margin: 0; padding: 0; width: 12%;">
                                        <div class="form-group no-margins" style="margin: 0; padding: 0;">
                                            <input id="subscriptionPlanItemDescription_1" name="subscriptionPlanItemDescription[1]" placeholder="5 Members" type="text" class="form-control subscriptionPlanItemDescription">
                                        </div>
                                    </td>

                                    <td class="no-margins no-padding" style="margin: 0; padding: 0; width: 12%;">
                                        <div class="form-group no-margins" style="margin: 0; padding: 0;">
                                            <input id="subscriptionPlanItemLimitType_1" name="subscriptionPlanItemLimitType[1]" placeholder="Storage, Users etc." type="text" class="form-control subscriptionPlanItemLimitType">
                                        </div>
                                    </td>

                                    <td class="no-margins no-padding" style="margin: 0; padding: 0; width: 12%;">
                                        <div class="form-group no-margins" style="margin: 0; padding: 0;">
                                            <input id="subscriptionPlanItemLimit_1" name="subscriptionPlanItemLimit[1]" placeholder="5 Users, 500GB" type="text" class="form-control subscriptionPlanItemLimit">
                                        </div>
                                    </td>

                                    <td class="no-margins no-padding" style="margin: 0; padding: 0; width: 12%;">
                                        <div class="form-group no-margins" style="margin: 0; padding: 0;">
                                            <input id="subscriptionPlanItemColor_1" name="subscriptionPlanItemColor[1]" placeholder="Text Color" type="color" class="form-control subscriptionPlanItemColor">
                                        </div>
                                    </td>

                                    <td class="no-margins no-padding" style="margin: 0; padding: 0; width: 12%;">
                                        <div class="form-group no-margins" style="margin: 0; padding: 0;">
                                            <input id="subscriptionPlanItemIcon_1" name="subscriptionPlanItemIcon[1]" placeholder="Font Awesome Icons" type="text" class="form-control subscriptionPlanItemIcon">
                                        </div>
                                    </td>

                                </tr>
                            @else
                                @foreach($items as $item)
                                    <tr id="rowSubscriptionPlanItem_{{  $loop->index + 1 }}" class="no-padding no-margins" style="margin-bottom: 0%">

                                        <td>
                                            {{  $loop->index + 1 }}
                                            <input type="hidden" name="subscriptionPlanItemId[{{  $loop->index + 1 }}]" value="{{ $item->id }}">
                                        </td>

                                        <td class="no-margins no-padding" style="margin: 0; padding: 0;">
                                            <div class="form-group no-margins" style="margin: 0; padding: 0;">
                                                <select class="form-control subscriptionPlanItemModule select2" id="subscriptionPlanItemModule_{{  $loop->index + 1 }}" name="subscriptionPlanItemModule[{{  $loop->index + 1 }}]">
                                                    @foreach($modules as $module => $module_title)
                                                        <option value="{{ $module }}" {{ ($module == $item->module)?'selected':'' }}>{{ $module_title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </td>

                                        <td class="no-margins no-padding" style="margin: 0; padding: 0; width: 12%;">
                                            <div class="form-group no-margins" style="margin: 0; padding: 0;">
                                                <input id="subscriptionPlanItemTitle_{{  $loop->index + 1 }}" name="subscriptionPlanItemTitle[{{  $loop->index + 1 }}]" value="{{ $item->title }}" placeholder="Members" type="text" class="form-control subscriptionPlanItemTitle">
                                            </div>
                                        </td>

                                        <td class="no-margins no-padding" style="margin: 0; padding: 0; width: 12%;">
                                            <div class="form-group no-margins" style="margin: 0; padding: 0;">
                                                <input id="subscriptionPlanItemDescription_{{  $loop->index + 1 }}" name="subscriptionPlanItemDescription[{{  $loop->index + 1 }}]" value="{{ $item->description }}" placeholder="5 Members" type="text" class="form-control subscriptionPlanItemDescription">
                                            </div>
                                        </td>

                                        <td class="no-margins no-padding" style="margin: 0; padding: 0; width: 12%;">
                                            <div class="form-group no-margins" style="margin: 0; padding: 0;">
                                                <input id="subscriptionPlanItemLimitType_{{  $loop->index + 1 }}" name="subscriptionPlanItemLimitType[{{  $loop->index + 1 }}]" value="{{ $item->limit_type }}" placeholder="Storage, Users etc." type="text" class="form-control subscriptionPlanItemLimitType">
                                            </div>
                                        </td>

                                        <td class="no-margins no-padding" style="margin: 0; padding: 0; width: 12%;">
                                            <div class="form-group no-margins" style="margin: 0; padding: 0;">
                                                <input id="subscriptionPlanItemLimit_{{  $loop->index + 1 }}" name="subscriptionPlanItemLimit[{{  $loop->index + 1 }}]" value="{{ $item->limits }}" placeholder="5 Users, 500GB" type="text" class="form-control subscriptionPlanItemLimit">
                                            </div>
                                        </td>

                                        <td class="no-margins no-padding" style="margin: 0; padding: 0; width: 12%;">
                                            <div class="form-group no-margins" style="margin: 0; padding: 0;">
                                                <input id="subscriptionPlanItemColor_{{  $loop->index + 1 }}" name="subscriptionPlanItemColor[{{  $loop->index + 1 }}]" value="{{ $item->color }}" placeholder="Text Color" type="color" class="form-control subscriptionPlanItemColor">
                                            </div>
                                        </td>

                                        <td class="no-margins no-padding" style="margin: 0; padding: 0; width: 12%;">
                                            <div class="form-group no-margins" style="margin: 0; padding: 0;">
                                                <input id="subscriptionPlanItemIcon_{{  $loop->index + 1 }}" name="subscriptionPlanItemIcon[{{  $loop->index + 1 }}]" value="{{ $item->icon }}" placeholder="Font Awesome Icons" type="text" class="form-control subscriptionPlanItemIcon">
                                            </div>
                                        </td>

                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        <input type="hidden" id="totalSubscriptionPlanItem" name="totalSubscriptionPlanItem" value="{{ $defaultEntries }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="portlet light bordered">
    <div class="portlet-body form">
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" id="btnConfirm" class="btn green">Save Changes</button>
                    <button type="reset" data-dismiss="modal" class="btn">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    modules = <?php echo json_encode($modules); ?>
</script>