<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionPlanModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_plan_modules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('subscription_plan_id')->unsigned()->nullable();

            $table->string('module')->nullable();
            $table->string('limit_type')->nullable();
            $table->string('limits')->nullable();

            $table->timestamps();

            $table->foreign('subscription_plan_id')->references('id')->on('subscription_plans')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_plan_modules');
    }
}
