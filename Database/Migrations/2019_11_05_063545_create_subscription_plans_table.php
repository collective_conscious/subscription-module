<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('unique_id')->unique();
            $table->bigInteger('user_id')->unsigned()->nullable();

            $table->string('title')->nullable();
            $table->string('name')->nullable();

            $table->string('currency')->nullable();
            $table->double('amount', 12, 4)->nullable();

            $table->string('duration')->nullable(); // monthly, quarterly

            $table->string('color')->nullable();

            $table->integer('status')->default(1);
            $table->integer('is_popular')->default(0);
            $table->integer('sales')->default(0);

            $table->timestamps();


            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_plans');
    }
}
